
#include "hw.hpp"

#include <assert.h>

void run_tests() {
  HW obj;
  assert( "Hello World!\n" == obj.format());
}

int main() {
  run_tests();
  return EXIT_SUCCESS;
}
