
#include "hw.hpp"
#include "fmt/format.h"

HW::HW() {}
HW::~HW() {}
void HW::print(FILE * file) { fmt::print(file, "{} {}!\n", "Hello", "World"); }
std::string HW::format() { return fmt::format("{} {}!\n", "Hello", "World"); }
