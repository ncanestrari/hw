
#pragma once

#include <cstdlib>
#include <string>

class HW {
public:
  HW();
  ~HW();
  void print(FILE * file);
  std::string format();
};

