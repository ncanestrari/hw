
include(ExternalProject)

ExternalProject_Add(
  libfmt
  DOWNLOAD_DIR ${CMAKE_SOURCE_DIR}/Libraries
  SOURCE_DIR ${CMAKE_SOURCE_DIR}/Libraries/libfmt
  BINARY_DIR ${CMAKE_BINARY_DIR}/libfmt
  INSTALL_DIR ${CMAKE_BINARY_DIR}/
  GIT_REPOSITORY https://github.com/fmtlib/fmt.git
  BUILD_ALWAYS true
  INSTALL_COMMAND ""
  )

