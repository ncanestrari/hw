

set(SRC src/hw.cpp)

include_directories(src)
include_directories(${CMAKE_SOURCE_DIR}/Libraries/libfmt/include)
link_directories(${CMAKE_BINARY_DIR}/libfmt)

add_library(hwlib ${SRC})
add_dependencies(hwlib libfmt)
target_link_libraries(hwlib fmt)

add_executable(ut_hw src/ut_hw.cpp)
target_link_libraries(ut_hw hwlib fmt)
